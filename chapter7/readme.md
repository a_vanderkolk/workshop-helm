# Chapter 7: Providing values to your Helm Chart
In the [chapter 5](../chapter5/readme.md) we have added ONE value to the Helm Chart. In this chapter we will make the Helm Chart more dynamic. 

I do not like to keep any configuration (that can be changed) in the template (yaml) files. For example, the number of replicas I want to extract from the deployment.yaml file. 

With changing values you can think of:
- Environment specific data (hostnames)
- Credentials
- Scaling configuration

The layout of the `values.yaml` file is relatively free (as long as it is valid yaml). You can create your own structure of your liking. 

I have some tips though:
- Group values for a specific object type (for example deployments, configmaps, services, etc.)
- If I have, for example, multiple ConfigMaps, I will create sub-groups in the ConfigMap group (for example `configmap.databasesettings` and `configmap.applicationsettings`)
- Create a `global` group for values for multiple objects
- Use _helpers.tpl for more complex structures
- Use documentation to describe the values (comments or a separate readme.md file)
- I like to create a separate group for the Image information


## Important
There are a few things you should know when working with the `values.yaml` file:

* Never, ever, ever, use dashes (`-`) in your variable names. This will give you all sorts of random errors without properly telling what is going wrong.


## Exercises
It's time to do some [exercises](./exercise/readme.md)

