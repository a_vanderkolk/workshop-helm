# Exercises Chapter 7

- [Exercises Chapter 7](#exercises-chapter-7)
  - [Exercise 1: Making the deployment more dynamic](#exercise-1-making-the-deployment-more-dynamic)
  - [Exercise 2: Changing the ConfigMap](#exercise-2-changing-the-configmap)
  - [Exercise 3: The service](#exercise-3-the-service)
  - [Exercise 4: Update the Chart.yaml file](#exercise-4-update-the-chartyaml-file)
  - [Exercise 5: Upgrade your release](#exercise-5-upgrade-your-release)

## Exercise 1: Making the deployment more dynamic
In the previous chapter we have seen that the deployment works. In this chapter we will make this 

We will make the following fields dynamic:
- spec.replicas
- spec.templates.containers.image

First we need to add the values to the `values.yaml` file: 

```yaml
deployment:
  replicas: 1

image:
  repository: quay.io/ha_van_der_kolk
  name: awesome-app
  tag: 1.0.0-SNAPSHOT
```

Now we can add the values to the `templates/deployments.yaml` file:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Values.application.name }}
  labels:
    app: {{ .Values.application.name }}
spec:
  replicas: {{ .Values.deployment.replicas }}
  # (...)
  template:
    # (...)
    spec:
      containers:
      - name: {{ .Values.application.name }}
        image: {{ .Values.image.repository }}/{{ .Values.image.name }}:{{ .Values.image.tag }}
        # (...)
```

As you can see, you string together multiple values. 

Check if your changes are working by running this command: 

```
helm install {your name}-awesome-app ./ --dry-run
```

## Exercise 2: Changing the ConfigMap
Add the group to the `values.yaml` file:

```yaml
configmap:
  message: |
    Space is limited
    In a haiku, so it's hard
    To finish what you
```

Now change the `templates/configmap.yaml`:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ .Values.application.name }}-config
data:
  personal.message: |
    {{- .Values.configmap.message | nindent 4 }}
```

Please note the `{{-` at the beginning and `nindent 4` at the end:
- the dash prevents creating a new line before the value
- the nindent corrects the formatting of the yaml

## Exercise 3: The service
At this moment we won't change the service. 

## Exercise 4: Update the Chart.yaml file
Increase the `version` in the `Chart.yaml` file

## Exercise 5: Upgrade your release
Now deploy the changes to your Helm Chart. 

```
helm upgrade {your name}-awesome-app ./
```
