# Chapter 8: Flow Control (using logic and _helpers.tpl)
One of the advantages of Helm is that you can use logic to determine values. There are also other functions to determine values / templates with. 

You are free to include functions to each of your template (yaml) files. But if you need a lot of if-statements or other logic, the yaml file will become unreadable. 

> Additional information can be found here: https://helm.sh/docs/chart_template_guide/control_structures/

- [Chapter 8: Flow Control (using logic and _helpers.tpl)](#chapter-8-flow-control-using-logic-and-_helperstpl)
  - [8.1. Logic inside template files](#81-logic-inside-template-files)
  - [8.2. Define (functions)](#82-define-functions)
  - [8.3. Using with](#83-using-with)
  - [8.4. Including files (the tpl function)](#84-including-files-the-tpl-function)


## 8.1. Logic inside template files
We can also include the logic inside a yaml file. 

```yaml
{{- if ne .Values.platform.type "kubernetes" -}}
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: {{ include "awesome-app.fullname" . }}
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
  - http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: {{ include "awesome-app.fullname" . }}
            port:
              number: 8080
{{- end -}}
```

You can also apply this method to a small part of the yaml file:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "testje.fullname" . }}
  # (...)
spec:
  {{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
  {{- end }}
  selector:
    matchLabels:
    # (...)
```



## 8.2. Define (functions)
It is possible to use simple logic to determine values. One example is determining the application name. 

```
{{- define "awesome-app.fullname" }}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name .Chart.Name }}
{{- end }}
```

The code above uses the value `fullnameOverride` if it is set, if this is not the case. It uses the `Chart.name` and `Release.name`. 

Now we can call this function in a template:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ include "awesome-app.fullname" . }}-config
data:
  personal.message: |
    {{- .Values.configmap.message | nindent 4 }}
```

The `include` function calls the function inside `_helpers.tpl`.




## 8.3. Using with
`with` is a very useful control object. It determines the current scope. 

Imagine we have a ConfigMap controlling the configuration of a logger

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ include "awesome-app.fullname" . }}-config
data:
  logger:
    level: {{ .Values.configmap.logger.level }}
    pattern: {{ .Values.configmap.logger.pattern }}
    location: {{ .Values.configmap.logger.filename }}
```

With it comes the following `values.yaml` file:

```yaml
```



As you can see we have included the part `.Values.configmap.logger` a couple of times. With `with` we can change the scope so that we do not have to add `.Values.configmap.logger` all the time. An example:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ include "awesome-app.fullname" . }}-config
data:
  {{- with .Values.configmap.logger }}
  logger:
    level: {{ .level }}
    pattern: {{ .pattern }}
    location: {{ .filename }}
  {{- end }}
```

As you can see, we now can refer to a variable inside `.Values.configmap.logger` by just the name of the variable. 


> **Note**: Once inside the scope 






## 8.4. Including files (the tpl function)
One of the things I find very useful is the `tpl` function. This allows us to include include files into our template. We even can include values inside these files. 

One use case is a (long) configuration file. If we place this directly in, for example a ConfigMap, the file could become unreadable. So what we can do is create a directory called `files` and create a `config.json` file. In this file we put the configuration we need. 

Any variables values can be changed by `{{ .Values.xyz }}` and we can add these values in the `values.yaml` file. 

So for example (`files/config.json`):
```json
{
  "configuration": {
    "hostname": "{{ .Values.config.database.hostname }}",
    "password": "{{ .Values.config.database.password }}"
  }
} 
```

We can now include this in our ConfigMap (which isn't save since we have defined a password)

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ include "awesome-app.fullname" . }}-configuration
data:
  config.json: {{ tpl (.Files.Get "files/config.json") . }}
```

This will include the content of the `config.json` file into the ConfigMap. This way you do not have a JSON string inside a YAML file. I think this is more manageable. 










