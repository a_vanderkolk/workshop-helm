# Exercises chapter 8

- [Exercises chapter 8](#exercises-chapter-8)
  - [Exercise 1: Create a function to determine the name](#exercise-1-create-a-function-to-determine-the-name)
  - [Exercise 2: Create a route and a ingress object](#exercise-2-create-a-route-and-a-ingress-object)
  - [Exercise 3: Changing values.yaml](#exercise-3-changing-valuesyaml)
  - [Exercise 4: Add if block to route.yaml](#exercise-4-add-if-block-to-routeyaml)
  - [Exercise 5: Add if block to ingress.yaml](#exercise-5-add-if-block-to-ingressyaml)
  - [Exercise 6: Create the Config.json file](#exercise-6-create-the-configjson-file)
  - [Exercise 7: Creating the secret](#exercise-7-creating-the-secret)

## Exercise 1: Create a function to determine the name
This is more or less shown in the chapter itself. 

First add the following function to `_helpers.tpl`

```
{{- define "awesome-app.fullname" }}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name .Chart.Name }}
{{- end }}
```

Now call the function in all the places you wish to include this name:
- `deployment.yaml`
- `configmap.yaml`
- `service.yaml`


## Exercise 2: Create a route and a ingress object
The route is used for openshift, the ingress for kubernetes. 

Create the file `templates/route.yaml` and add the following content:

```yaml
apiVersion: v1
kind: Route
metadata:
  name: {{ include "awesome-app.fullname" . }}
spec:
  to:
    kind: Service
    name: {{ include "awesome-app.fullname" . }}
```

This will generate an automatic hostname and forwards traffic to the Service

Now create the file `templates\ingress.yaml`. Add the following content:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: {{ include "awesome-app.fullname" . }}
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
  - http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: {{ include "awesome-app.fullname" . }}
            port:
              number: 8080
```

Like the route, we do not add an address at this moment. 

## Exercise 3: Changing values.yaml
Unfortunately we cannot deploy the Ingress and the Route on the same cluster - namespace. We need to deploy the route if we deploy on OpenShift, we need to deploy the Ingress on OpenShift.

First thing we will do is change the `values.yaml` file. Add the following group:


```yaml
platform:
  type: openshift
```

This will set the default `platform.type` to "openshift". 

## Exercise 4: Add if block to route.yaml
Only on OpenShift we need to deploy the route. Therefore we will add the following if block to the file `templates/route.yaml`:

```yaml
{{- if eq .Values.platform.type "openshift" -}}
apiVersion: v1
kind: Route
# (...)
# rest of your yaml
{{- end -}}
```

This will only include the 'yaml' if the value of `platform.type` is 'openshift'.

## Exercise 5: Add if block to ingress.yaml
We need to do the same thing to `templates/ingress.yaml`

```yaml
{{- if eq .Values.platform.type "kubernetes" -}}
apiVersion: networking.k8s.io/v1
kind: Ingress
# (...)
# rest of your yaml
{{- end -}}
```

## Exercise 6: Create the Config.json file
Create the folder `files` in the root of your helm project. 

```
mkdir files
```

Now create the file `config.yaml` and add the following content.

```yaml
configuration:
  database:
    hostname: {{ .Values.secret.configuration_yaml.database.host }}
    port: 
    username:
    password: 

```

## Exercise 7: Creating the secret


