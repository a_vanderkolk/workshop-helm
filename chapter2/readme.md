# Chapter 2: Installing Helm

- [Chapter 2: Installing Helm](#chapter-2-installing-helm)
  - [2.1. Installing on Unix](#21-installing-on-unix)
  - [2.2. Windows](#22-windows)


Installing Helm is described on this page:
https://helm.sh/docs/intro/install/

## 2.1. Installing on Unix
If you have access to the internet you can just simply run the following commands:

```
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
```

Otherwise you need to download the tgz-file for linux (amd64) here: https://github.com/helm/helm/releases
Then upload the the tgz-file to your host
Finally run these commands:

```
# https://get.helm.sh/helm-v3.6.0-linux-amd64.tar.gz
tar -zxvf helm-v{helm version}-linux-amd64.tar.gz

sudo cp linux-amd64/helm /usr/local/bin/helm
```

Test the installation by running: `helm version`

## 2.2. Windows
The installation on Windows is more or less the same. 
1. Download the zip-file from https://github.com/helm/helm/releases
2. Unzip the files to a location of your liking (for example c:\ws\helm)
3. Add the folder to the PATH variable

Test the installation by running: `helm version`