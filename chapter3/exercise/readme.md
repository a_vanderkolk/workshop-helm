# Exercises Chapter 3

- [Exercises Chapter 3](#exercises-chapter-3)
  - [Exercise 1](#exercise-1)

## Exercise 1 

Simply run the command:

```
helm create {your name}-awesome-app
```

This will create a selection of files. [Chapter 4](../../chapter4/readme.md) I will explain the contents of the Helm Chart