# Chapter 3: Creating your first Helm Chart
By running the `helm create` command, Helm will create a set of folders and files. 

A Helm Chart is basically just a collection of files and folders. 

## 3.1. Exercise
See [exercise](exercise/readme.md)