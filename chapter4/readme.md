# Chapter 4: Contents of a Helm Chart
In the Chapter 3 we have created our first Helm Chart. This chapter will describe the contents of a Helm Chart. 

- [Chapter 4: Contents of a Helm Chart](#chapter-4-contents-of-a-helm-chart)
  - [4.1. Chart.yaml](#41-chartyaml)
  - [4.2. charts](#42-charts)
  - [4.3. templates](#43-templates)
    - [4.3.1. .yaml files](#431-yaml-files)
    - [4.3.2. NOTES.txt](#432-notestxt)
    - [4.3.3. _helpers.tpl](#433-_helperstpl)
    - [4.3.4. tests](#434-tests)
  - [4.4. values.yaml](#44-valuesyaml)

```
.
└── antons-awesome-app
    ├── Chart.yaml
    ├── charts
    ├── templates
    │   ├── NOTES.txt
    │   ├── _helpers.tpl
    │   ├── deployment.yaml
    │   ├── hpa.yaml
    │   ├── ingress.yaml
    │   ├── service.yaml
    │   ├── serviceaccount.yaml
    │   └── tests
    │       └── test-connection.yaml
    └── values.yaml
```

## 4.1. Chart.yaml
This file contains the information about the Chart. With all the comments deleted, it looks like this:

```yaml
apiVersion: v2
name: antons-awesome-app
description: A Helm chart for Kubernetes
type: application
version: 0.1.0
appVersion: "1.16.0"
```

For the complete description of the fields see: https://helm.sh/docs/topics/charts/#the-chartyaml-file

| Field       | Required                                                                                                     | Description                                                                                                                                                               |
| :---------- | :----------------------------------------------------------------------------------------------------------- | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| apiVersion  | yes                                                                                                          | This fields sets the version of Helm to use. `v2` means Helm version 3.                                                                                                   |
| name        | yes                                                                                                          | This is the name of the chart. By default this is part of the name of the objects                                                                                         |
| description | no                                                                                                           | A description of the goal of this chart                                                                                                                                   |
| type        | no                                                                                                           | There are two options: `application` the helm chart is used to install objects, `library` the helm chart contains functions for the other helm charts (out of this scope) |
| version     | yes                                                                                                          | The version of the Helm Chart. Ideally this version is raised every change to the Chart. This version could be used when rolling back a Helm Chart Release                |
| appVersion  | This is the version of the app the Helm Chart will install. For example the version from your `pom.xml` file |

## 4.2. charts
When using subcharts, this folder is used. 

## 4.3. templates
This folder will contain the following items:
* NOTES.txt
* _helpers.tpl
* .yaml files


### 4.3.1. .yaml files
These are the Kubernetes objects we are deploying on the cluster. If we run the `helm create` command, Helm will create some basic example files for us. It is your choice to use them or to delete them. 

Each `.yaml` file will be deployed on the cluster. 

### 4.3.2. NOTES.txt
The content of this file is shown when you deploy the Helm Chart. You can add Post Install instructions, the URL to your app, etc. 

### 4.3.3. _helpers.tpl
This file contains basically functions to help you determine values. It enables you to use some basic logic methods and other functions. In [Chapter 7](../chapter7/readme.md) details about using this functions will be described.

### 4.3.4. tests
This folder will contain tests to run after installing the Helm Chart. This is outside the scope of this workshop.

## 4.4. values.yaml
This is `values.yaml` with a lowercase v. This file will contain all the parameters with the default values you are going to use in your templates. 

The user can overwrite these values by:
- providing a (partial) values file
- providing parameters

