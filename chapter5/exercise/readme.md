# Exercises chapter 5

- [Exercises chapter 5](#exercises-chapter-5)
  - [Exercise 1: Empty your Helm Chart](#exercise-1-empty-your-helm-chart)
  - [Exercise 2: Create the deployment object](#exercise-2-create-the-deployment-object)
  - [Exercise 3: Add your application name to the values.yaml file](#exercise-3-add-your-application-name-to-the-valuesyaml-file)
  - [Exercise 4: Create the Service](#exercise-4-create-the-service)
  - [Exercise 5: Create the ConfigMap](#exercise-5-create-the-configmap)
  - [Exercise 6: Update the Chart.yaml](#exercise-6-update-the-chartyaml)

## Exercise 1: Empty your Helm Chart
First delete everything from your Helm Chart

```
cd c05-{your name}-awesome-app

# delete the (empty) charts directory
rm -rf charts
# delete the tests
rm -rf templates/tests
# delete all the yaml files
rm templates/*.yaml

# empty the values file 
echo '# Todo: add values here' > values.yaml

# empty the _helpers.tpl file
echo '# Todo: add helpers here' > templates/_helpers.tpl

# empty the notes
echo '{{ .Chart.Name }} successfully installed!' > templates/NOTES.txt
```

## Exercise 2: Create the deployment object
The first step is to create a deployment object. This object will start our application. 

Create the file `templates/deployment.yaml` and add the following content

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Values.application.name }}
  labels:
    app: {{ .Values.application.name }}
spec:
  replicas: 1
  selector:
    matchLabels:
      app: {{ .Values.application.name }}
  template:
    metadata:
      labels:
        app: {{ .Values.application.name }}
    spec:
      containers:
      - name: {{ .Values.application.name }}
        image: quay.io/ha_van_der_kolk/awesome-app:latest
        ports:
        - containerPort: 8080
        env:
        - name: PERSONAL_MESSAGE
          valueFrom:
            configMapKeyRef:
              name: {{ .Values.application.name }}-config
              key: personal.message
```

Note; I am using a environment variable from the ConfigMap. How this works is outside the scope of this workshop


As you can see we are using the following syntax inside the yaml file `{{ .Values.application.name }}`. This will read the value from `application.name` from the values.yaml file.

So next step is to add this to the values.yaml file

## Exercise 3: Add your application name to the values.yaml file
Open the file `values.yaml`

Now add the following content:

```yaml
application:
  name: {your name}-awesome-app
```

Note; if you are using the Helm Intellisens plugin, vscode will automatically read the values.yaml values. 

## Exercise 4: Create the Service
The service will route the traffic to the pods. 

Create the file `templates/service.yaml`

Add the following content: 

```yaml
apiVersion: v1
kind: Service
metadata:
  name: {{ .Values.application.name }}
spec:
  selector:
    app: {{ .Values.application.name }}
  type: ClusterIP
  ports:
    - name: 8080-tcp
      port: 8080
      protocol: TCP
      targetPort: 8080
```

## Exercise 5: Create the ConfigMap
The last object is the ConfigMap. 

Create the file `templates/configmap.yaml`

Add the following content: 

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ .Values.application.name }}-config
data:
  personal.message: |
    Space is limited
    In a haiku, so it's hard
    To finish what you
```

NOTE; In the deployment config we have specified the name of this ConfigMap. The name in `metadata.name` must be equal to this name. 

The `|` in the yaml tells that the next lines should be a String



## Exercise 6: Update the Chart.yaml
Now that we have made changes to the contents of the Helm Chart, it is good practice to increase the value of `version` in `Chart.yaml`.

Also change the value `appVersion`

```yaml
apiVersion: v2
name: antons-awesome-app
description: A Helm chart for Kubernetes
type: application
version: 0.1.1
appVersion: "1.0.0-SNAPSHOT"
```

The application version of the Quarkus app is `1.0.0-SNAPSHOT`. The appVersion takes a String value, so you can basically put in everything. Please don't though.

