# Exercises chapter 6

- [Exercises chapter 6](#exercises-chapter-6)
  - [Exercise 1: Install the Helm Chart](#exercise-1-install-the-helm-chart)
  - [Exercise 2: Change the number of replicas](#exercise-2-change-the-number-of-replicas)
  - [Exercise 3: Perform a rollback](#exercise-3-perform-a-rollback)
  - [Exercise 4: Uninstall the app](#exercise-4-uninstall-the-app)

## Exercise 1: Install the Helm Chart
In the previous chapter you have created the first version of the Helm Chart. In this exercise we will deploy the Helm Chart for the first time. 

First check the objects Helm will create for you:
```
# Go into the Helm Chart folder
cd {your name}-awesome-app

helm install {your name}-awesome-app ./ --dry-run
```

This will provide you with a list of yamls. These are the yamls which will be pushed to the namespace. So any dynamic values will be filled in. 

If this looks all right, we can actually deploy the Helm Chart

```
helm install {your name}-awesome-app ./
```


## Exercise 2: Change the number of replicas
In the file `templates/deployment.yaml` we have specified that the application needs one replica.

Change the value to '2':

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Values.application.name }}
  labels:
    app: {{ .Values.application.name }}
spec:
  replicas: 2
  # (...)
```

Save the changes. 

**NOTE**: Do not forget to update the Chart.yaml

Now upgrade your Helm Release:

```
helm upgrade {your name}-awesome-app ./
```

This should spin up a second pod for your app. 


## Exercise 3: Perform a rollback
After scaling up the number of replicas you are running into quota issues on your namespace. Therefore you decide to rollback the last change. 

Run the following command: 

```
helm rollback {your name}-awesome-app
```

Check if the number of pods is scaled down again. 

Note: if you wish to make this rollback permanent, change the Helm Chart

## Exercise 4: Uninstall the app
You decide that the application is not ready for production yet. So you uninstall the application. You can do this by running this command: 

```
helm uninstall {your name}-awesome-app
```

