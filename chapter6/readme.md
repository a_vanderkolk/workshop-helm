# Chapter 6: Deploy, Upgrade, Rollback and Uninstall
In the previous chapter you have created your very first Helm Chart for an application. 

There are several commands available to deploy our Helm Chart to the cluster. 

- [Chapter 6: Deploy, Upgrade, Rollback and Uninstall](#chapter-6-deploy-upgrade-rollback-and-uninstall)
  - [6.1. Install the Helm Chart](#61-install-the-helm-chart)
    - [6.1.1. Overwriting values](#611-overwriting-values)
  - [6.2. Upgrade](#62-upgrade)
  - [6.3. Rollback](#63-rollback)
  - [6.4. Uninstall](#64-uninstall)
  - [6.5. Exercises](#65-exercises)

## 6.1. Install the Helm Chart
With the `helm install` command we will install the Helm Chart to the cluster for the **first** time. 

The complete command is:
```
helm install {release name} {location of the Helm Chart}
```

The release name is the name of the release. I prefer to use the application name. 
The location of the helm chart is the place on your disk.

So for example:
```
cd antons-awesome-app
helm install antons-awesome-app ./
```

TIP: If you are stuck, you can always run the command `helm instal -help`

### 6.1.1. Overwriting values
As mentioned before, we can overwrite values inside the Helm Chart. One way to do this by using the `--set` parameter. 

In the previous chapter we have defined one parameter (`application.name`). If we wish to overwrite this value, we can provoke the following command: 

```
helm install antons-awesome-app ./ --set application.name=henks-awesome-app
```

You can add as much parameters as you want. If you need to overwrite many parameters, you can also use a yaml file. 

I like to follow this workflow:
1. Create a copy of the `values.yaml` file from the Helm Chart to a separate location
2. Remove all the values you do not want to overwrite
3. Change the values you wish to overwrite
4. Run the install command again

```
helm install antons-awesome-app ./ -f ~/dev-values.yaml
```

## 6.2. Upgrade
If the Helm Chart is already installed on the cluster (and in the Namespace) you need to run the `helm upgrade` command. 

This command works the same as the `install` command. 


## 6.3. Rollback
If you changed something in the configuration of the Kubernetes objects, and it appears not to be working, this command will rollback to the previous version. 

With the following command you can see the history of a Helm Release.

```
helm history {your release name}
```

You can rollback with the following command 

```
helm rollback {your release name}
```



## 6.4. Uninstall
The final command is `helm uninstall`. This will uninstall all the contents of the Helm Chart from the namespace. 

```
helm uninstall {your release name}
```

Be careful when using this command. This will also delete Persistent Volumes

## 6.5. Exercises
Continue to the [exercises](./exercise/readme.md)