# Chapter 1: What is Helm?

Helm provides a way of templating Kubernetes Objects. 

> Helm helps you manage Kubernetes applications — Helm Charts help you define, install, and upgrade even the most complex Kubernetes application.

> Charts are easy to create, version, share, and publish — so start using Helm and stop the copy-and-paste.

Helm allows us to:
- Customize a set of Kubernetes Objects on the fly by:
  - Using parameters
  - Using parameter files
  - Using 'logic'
- Update a deployment of a set of objects
- Rollback to previous version of the set
- Share Helm Charts by using repositories
